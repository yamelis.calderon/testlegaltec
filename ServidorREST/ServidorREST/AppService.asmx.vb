﻿Imports System.Web.Services
Imports System.ComponentModel


' Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://localhost/")>
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class AppService
    Inherits WebService
    Dim _multaController As New MultaController
    <WebMethod()>
    Public Function GetRegistrosPorPatente(ByVal Patente As String) As DataTable
        Try

            Return _multaController.GetRegistrosPorPatente(Patente)

        Catch ex As Exception
            Return New DataTable
        End Try
    End Function
    <WebMethod()>
    Public Function InsertMulta(ByVal _patente As String, ByVal _motivo As String, ByVal _montoMulta As Decimal, ByVal _intereses As Decimal) As String
        Try
            Dim _multa As Multa = New Multa
            Dim _vehiculo As Vehiculo = New Vehiculo
            _vehiculo.Patente = _patente
            _multa.Vehiculo = _vehiculo
            _multa.Motivo = _motivo
            _multa.Monto = _montoMulta
            _multa.Intereses = _intereses
            Return _multaController.InsertMulta(_multa)
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function
    <WebMethod()>
    Public Function InsertPagoMulta(ByVal _rutConductor As String, ByVal _idMulta As Int32, ByVal _montoPago As Decimal) As String
        Try
            Dim _multaPago As Multa = New Multa
            Dim _vehiculo As Vehiculo = New Vehiculo
            Dim _conductor As Conductor = New Conductor

            _conductor.RutConductor = _rutConductor
            _vehiculo.Conductor = _conductor
            _multaPago.Id = _idMulta
            _multaPago.Vehiculo = _vehiculo
            Return _multaController.InsertPagoMulta(_multaPago, _montoPago)
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function


End Class