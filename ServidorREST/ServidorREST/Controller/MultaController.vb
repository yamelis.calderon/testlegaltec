﻿Public Class MultaController
    Dim _multaDao As New MultaDao

    Public Sub MultaController()

    End Sub

    Public Function GetRegistrosPorPatente(Patente As String) As DataTable
        Try
            Dim dt = _multaDao.GetRegistrosPorPatente(Patente)
            Return dt

        Catch ex As Exception
            Return New DataTable
        End Try
    End Function

    Public Function InsertMulta(ByVal _multa As Multa) As String
        Try
            Return _multaDao.InsertMulta(_multa)
        Catch ex As Exception
            Return "Error" + ex.Message
        End Try
    End Function
    Public Function InsertPagoMulta(ByVal _multa As Multa, ByVal _montoPago As Decimal) As String
        Try
            Return _multaDao.InsertPagoMulta(_multa, _montoPago)
        Catch ex As Exception
            Return "Error" + ex.Message
        End Try
    End Function

End Class
