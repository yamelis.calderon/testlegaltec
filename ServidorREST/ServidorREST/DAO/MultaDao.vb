﻿Imports System.Data.SqlClient

Public Class MultaDao
     Dim conexion As New SqlConnection(ConfigurationManager.ConnectionStrings("conexion").ConnectionString)
    Public Sub MultaDao()

    End Sub

    Public Function GetRegistrosPorPatente(ByVal Patente As String) As DataTable
        Dim dt As New DataTable("tabla")
        Dim cnx As New SqlConnection(conexion.ConnectionString)

        Try
            Dim comandoSql As New SqlCommand("GetRegistroPorPatente", cnx)
            comandoSql.CommandType = CommandType.StoredProcedure
            comandoSql.Parameters.AddWithValue("@Patente", Patente)
            Dim adaptadorSql As New SqlDataAdapter(comandoSql)
            adaptadorSql.SelectCommand = comandoSql
            adaptadorSql.Fill(dt)
            cnx.Close()
            cnx.Dispose()
            Return dt

        Catch ex As Exception

        End Try




        Return dt


    End Function

    Public Function InsertMulta(ByVal _multa As Multa) As String
        Dim _mensaje As String = ""
        Try
            Dim cnx As New SqlConnection(conexion.ConnectionString)
            Dim comandoSql As New SqlCommand("dbo.InsertMulta", cnx)
            comandoSql.CommandType = CommandType.StoredProcedure
            comandoSql.Parameters.AddWithValue("@PATENTE", _multa.Vehiculo.Patente)
            comandoSql.Parameters.AddWithValue("@MOTIVO", _multa.Motivo)
            comandoSql.Parameters.AddWithValue("@MONTOMULTA", _multa.Monto)
            comandoSql.Parameters.AddWithValue("@INTERES", _multa.Intereses)
            cnx.Open()
            comandoSql.ExecuteNonQuery()
            _mensaje = "Multa registrada con éxito"
            cnx.Close()
            cnx.Dispose()
        Catch ex As Exception
            _mensaje = "Ocurrio un error registrando la Multa" + ex.Message
        End Try
        Return _mensaje
    End Function


    Public Function InsertPagoMulta(ByVal _multa As Multa, _montoPago As Decimal) As String
        Dim _mensaje As String = ""
        Try
            Dim cnx As New SqlConnection(conexion.ConnectionString)
            Dim comandoSql As New SqlCommand("dbo.InsertPagoMulta", cnx)
            comandoSql.CommandType = CommandType.StoredProcedure
            comandoSql.Parameters.AddWithValue("@RUTCONDUCTOR", _multa.Vehiculo.Conductor.RutConductor)
            comandoSql.Parameters.AddWithValue("@IDMULTA", _multa.Id)
            comandoSql.Parameters.AddWithValue("@MONTOMULTA", _montoPago)
            cnx.Open()
            comandoSql.ExecuteNonQuery()
            _mensaje = "Multa pagada con éxito"
            cnx.Close()
            cnx.Dispose()
        Catch ex As Exception
            _mensaje = "Ocurrio un error registrando el pago de la Multa" + ex.Message
        End Try
        Return _mensaje
    End Function

End Class
