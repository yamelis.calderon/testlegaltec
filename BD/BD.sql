USE [master]
GO
/****** Object:  Database [pruebaLegaltec]    Script Date: 14/02/2021 22:17:46 ******/
CREATE DATABASE [pruebaLegaltec]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'pruebaLegaltec', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\pruebaLegaltec.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'pruebaLegaltec_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\pruebaLegaltec_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [pruebaLegaltec] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [pruebaLegaltec].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [pruebaLegaltec] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [pruebaLegaltec] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [pruebaLegaltec] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [pruebaLegaltec] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [pruebaLegaltec] SET ARITHABORT OFF 
GO
ALTER DATABASE [pruebaLegaltec] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [pruebaLegaltec] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [pruebaLegaltec] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [pruebaLegaltec] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [pruebaLegaltec] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [pruebaLegaltec] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [pruebaLegaltec] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [pruebaLegaltec] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [pruebaLegaltec] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [pruebaLegaltec] SET  DISABLE_BROKER 
GO
ALTER DATABASE [pruebaLegaltec] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [pruebaLegaltec] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [pruebaLegaltec] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [pruebaLegaltec] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [pruebaLegaltec] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [pruebaLegaltec] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [pruebaLegaltec] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [pruebaLegaltec] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [pruebaLegaltec] SET  MULTI_USER 
GO
ALTER DATABASE [pruebaLegaltec] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [pruebaLegaltec] SET DB_CHAINING OFF 
GO
ALTER DATABASE [pruebaLegaltec] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [pruebaLegaltec] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [pruebaLegaltec] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [pruebaLegaltec] SET QUERY_STORE = OFF
GO
USE [pruebaLegaltec]
GO
/****** Object:  User [Yamelis]    Script Date: 14/02/2021 22:17:46 ******/
CREATE USER [Yamelis] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [yame]    Script Date: 14/02/2021 22:17:46 ******/
CREATE USER [yame] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[db_accessadmin]
GO
/****** Object:  User [Usuario]    Script Date: 14/02/2021 22:17:46 ******/
CREATE USER [Usuario] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [admin]    Script Date: 14/02/2021 22:17:46 ******/
CREATE USER [admin] FOR LOGIN [admin] WITH DEFAULT_SCHEMA=[db_owner]
GO
ALTER ROLE [db_owner] ADD MEMBER [yame]
GO
ALTER ROLE [db_owner] ADD MEMBER [Usuario]
GO
ALTER ROLE [db_owner] ADD MEMBER [admin]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [admin]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [admin]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [admin]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [admin]
GO
ALTER ROLE [db_datareader] ADD MEMBER [admin]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [admin]
GO
ALTER ROLE [db_denydatareader] ADD MEMBER [admin]
GO
ALTER ROLE [db_denydatawriter] ADD MEMBER [admin]
GO
/****** Object:  Table [dbo].[Conductor]    Script Date: 14/02/2021 22:17:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Conductor](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RutConductor] [varchar](50) NOT NULL,
	[Nombre] [varchar](100) NULL,
	[Apellido] [varchar](100) NULL,
	[Email] [varchar](100) NULL,
	[IndVigencia] [varchar](1) NULL,
 CONSTRAINT [PK_Conductor] PRIMARY KEY CLUSTERED 
(
	[RutConductor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetallePago]    Script Date: 14/02/2021 22:17:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetallePago](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdPago] [varchar](100) NULL,
	[IdMulta] [int] NULL,
	[MontoPago] [money] NULL,
	[indVigencia] [varchar](1) NULL,
 CONSTRAINT [PK_DetallePago] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Estatus]    Script Date: 14/02/2021 22:17:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Estatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](50) NULL,
	[IndVigencia] [varchar](1) NULL,
 CONSTRAINT [PK_Estatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Marca]    Script Date: 14/02/2021 22:17:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Marca](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](200) NULL,
	[IndVigencia] [varchar](1) NULL,
 CONSTRAINT [PK_Marca] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Modelo]    Script Date: 14/02/2021 22:17:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Modelo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdMarca] [int] NULL,
	[Descripcion] [varchar](200) NULL,
	[Year] [int] NULL,
	[IndVigencia] [varchar](1) NULL,
	[IdPermiso] [int] NULL,
 CONSTRAINT [PK_Modelo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Multa]    Script Date: 14/02/2021 22:17:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Multa](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PatenteVehiculo] [varchar](50) NULL,
	[MotivoMulta] [varchar](max) NULL,
	[MontoMulta] [money] NULL,
	[InteresReajuste] [money] NULL,
	[IdEstatus] [int] NULL,
	[Fecha] [datetime] NULL,
	[IndVigencia] [varchar](1) NULL,
 CONSTRAINT [PK_Multa] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pago]    Script Date: 14/02/2021 22:17:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pago](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RutConductor] [varchar](50) NULL,
	[Fecha] [datetime] NULL,
	[IdTipoPago] [int] NULL,
	[IndVigencia] [varchar](1) NULL,
 CONSTRAINT [PK_Pago] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Permiso]    Script Date: 14/02/2021 22:17:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permiso](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ValorPermiso] [money] NULL,
	[Fecha] [datetime] NULL,
	[IndVigencia] [varchar](1) NULL,
 CONSTRAINT [PK_Permiso] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoPago]    Script Date: 14/02/2021 22:17:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoPago](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[IndVigencia] [varchar](1) NULL,
 CONSTRAINT [PK_TipoPago] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Vehiculo]    Script Date: 14/02/2021 22:17:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vehiculo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Patente] [varchar](50) NOT NULL,
	[IdModelo] [int] NULL,
	[RutConductor] [varchar](50) NULL,
	[IndVigencia] [varchar](1) NULL,
 CONSTRAINT [PK_Vehiculo] PRIMARY KEY CLUSTERED 
(
	[Patente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DetallePago]  WITH CHECK ADD  CONSTRAINT [FK_DetallePago_Multa] FOREIGN KEY([IdMulta])
REFERENCES [dbo].[Multa] ([Id])
GO
ALTER TABLE [dbo].[DetallePago] CHECK CONSTRAINT [FK_DetallePago_Multa]
GO
ALTER TABLE [dbo].[Modelo]  WITH CHECK ADD  CONSTRAINT [FK_Modelo_Modelo] FOREIGN KEY([IdMarca])
REFERENCES [dbo].[Marca] ([Id])
GO
ALTER TABLE [dbo].[Modelo] CHECK CONSTRAINT [FK_Modelo_Modelo]
GO
ALTER TABLE [dbo].[Multa]  WITH CHECK ADD  CONSTRAINT [FK_Multa_Estatus] FOREIGN KEY([IdEstatus])
REFERENCES [dbo].[Estatus] ([Id])
GO
ALTER TABLE [dbo].[Multa] CHECK CONSTRAINT [FK_Multa_Estatus]
GO
ALTER TABLE [dbo].[Multa]  WITH CHECK ADD  CONSTRAINT [FK_Multa_Vehiculo] FOREIGN KEY([PatenteVehiculo])
REFERENCES [dbo].[Vehiculo] ([Patente])
GO
ALTER TABLE [dbo].[Multa] CHECK CONSTRAINT [FK_Multa_Vehiculo]
GO
ALTER TABLE [dbo].[Pago]  WITH CHECK ADD  CONSTRAINT [FK_Pago_TipoPago] FOREIGN KEY([IdTipoPago])
REFERENCES [dbo].[TipoPago] ([Id])
GO
ALTER TABLE [dbo].[Pago] CHECK CONSTRAINT [FK_Pago_TipoPago]
GO
ALTER TABLE [dbo].[Vehiculo]  WITH CHECK ADD  CONSTRAINT [FK_Vehiculo_Conductor] FOREIGN KEY([RutConductor])
REFERENCES [dbo].[Conductor] ([RutConductor])
GO
ALTER TABLE [dbo].[Vehiculo] CHECK CONSTRAINT [FK_Vehiculo_Conductor]
GO
ALTER TABLE [dbo].[Vehiculo]  WITH CHECK ADD  CONSTRAINT [FK_Vehiculo_Modelo] FOREIGN KEY([IdModelo])
REFERENCES [dbo].[Modelo] ([Id])
GO
ALTER TABLE [dbo].[Vehiculo] CHECK CONSTRAINT [FK_Vehiculo_Modelo]
GO
/****** Object:  StoredProcedure [dbo].[GetDatosPorPatente]    Script Date: 14/02/2021 22:17:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[GetDatosPorPatente] 
(
 @Patente	varchar(50)
)
AS
/* '===============================================================          
  '   NOMBRE                : [dbo].[GetDatosPorPatente] 
  '   FECHA CREACIÓN        : 12-02-2021
  '	  CREADO POR			: YAMELIS CALDERON
  '   CREADO PARA           : PRUEBA TÉCNICA
  '   FUNCIÓN               : OBTENER DATOS DEL VEHICULO POR PATENTE
  '   VERSIÓN               : 1.0.0
  '   MODIFICADO EN         : 
  '   MODIFICADO POR        : 
  '   RAZÓN DE MODIFICACIÓN : 
  '===============================================================*/    
SET NOCOUNT ON
DECLARE @ERRORMESSAGE NVARCHAR(4000)  
DECLARE @ERRORSEVERITY INT  
DECLARE @ERRORSTATE INT  

BEGIN TRY
   
    SELECT  V.Patente, CONCAT(M.Descripcion, ' ', MD.Descripcion) AS DescripcionVehiculo,
			MD.Year,C.RutConductor , CONCAT(C.Nombre ,' ', C.Apellido) AS Conductor,
			MT.Valor , SUM(MT.Valor) AS Total , E.Descripcion AS EstatusMulta    
	FROM  Vehiculo V
	INNER JOIN Marca M ON V.IdMarca = M.Id
	INNER JOIN MODELO MD ON MD.Id = V.IdModelo
	INNER JOIN Conductor C ON C.RutConductor = V.RutConductor
	INNER JOIN Multa MT ON MT.PatenteVehiculo = V.Patente
	INNER JOIN Estatus E ON E.Id = MT.IdEstatus
	WHERE V.Patente = 'BZXJ51' 
	GROUP BY V.Patente,M.Descripcion,MD.Descripcion,MD.Year,c.RutConductor,C.NOMBRE ,C.Apellido,MT.Valor,E.Descripcion


    
END TRY

BEGIN CATCH
    
  SET @ERRORMESSAGE = 'ERROR ' + CAST(ERROR_NUMBER() AS VARCHAR(10)) + '. LINEA ' + CAST(ERROR_LINE() AS VARCHAR(10)) + '. ' + ERROR_MESSAGE()
  SET @ERRORSEVERITY = ERROR_SEVERITY()  
  SET @ERRORSTATE = ERROR_STATE()  
  GOTO ERROR
END CATCH

SET NOCOUNT OFF

RETURN
ERROR:
  IF XACT_STATE() <> 0 ROLLBACK TRAN
  RAISERROR (@ERRORMESSAGE, @ERRORSEVERITY, @ERRORSTATE)

GO
/****** Object:  StoredProcedure [dbo].[GetRegistroPorPatente]    Script Date: 14/02/2021 22:17:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[GetRegistroPorPatente] 
(
 @Patente	varchar(50)
)
AS
/* '===============================================================          
  '   NOMBRE                : [dbo].[GetRegistroPorPatente] 'CJCT94'  
  '   FECHA CREACIÓN        : 12-02-2021
  '	  CREADO POR			: YAMELIS CALDERON
  '   CREADO PARA           : PRUEBA TÉCNICA
  '   FUNCIÓN               : OBTENER DATOS DEL VEHICULO POR PATENTE
  '   VERSIÓN               : 1.0.0
  '   MODIFICADO EN         : 
  '   MODIFICADO POR        : 
  '   RAZÓN DE MODIFICACIÓN : 
  '===============================================================*/    
SET NOCOUNT ON
DECLARE @ERRORMESSAGE NVARCHAR(4000)  
DECLARE @ERRORSEVERITY INT  
DECLARE @ERRORSTATE INT  

BEGIN TRY
	
	 SELECT V.Patente ,
		   CONCAT(MR.Descripcion,' ',
		   MD.Descripcion) AS Modelo,
		   V.RutConductor , 
		   CONCAT(C.Nombre, ' ', C.Apellido) AS NombreConductor,
		   P.ValorPermiso, 
		   ISNULL(M.Id,0) AS IdMulta,
		   CASE WHEN  ISNULL(M.Id,0) = 0 THEN '' ELSE CONCAT('Nro. ',V.Patente, ' - ', M.Id) END AS NroMulta,
		   CASE WHEN M.IdEstatus = 2 THEN 0 ELSE ISNULL(M.MontoMulta,0) END AS MontoMulta,
		   CASE WHEN M.IdEstatus  = 2 THEN 0 ElSE ISNULL(M.InteresReajuste,0) END As InteresReajuste,
		   SUM(P.ValorPermiso) + SUM(ISNULL(M.MontoMulta,0)) + SUM(ISNULL(M.InteresReajuste,0)) as Subtotal
	FROM Vehiculo V 
	INNER JOIN MODELO MD ON V.IdModelo   = MD.Id 
	INNER JOIN Marca MR ON MD.IdMarca = MR.Id 
	INNER JOIN Permiso  P ON P.Id = MD.IdPermiso
	INNER JOIN Conductor C ON V.RutConductor = C.RutConductor
	LEFT JOIN Multa M ON M.PatenteVehiculo = V.Patente 
	WHERE V.Patente = @Patente 
	GROUP BY  V.Patente ,MR.Descripcion, MD.Descripcion, V.RutConductor,
	C.Nombre,C.Apellido,P.ValorPermiso,M.MontoMulta,M.InteresReajuste, M.IdEstatus,M.Id
    
END TRY

BEGIN CATCH
    
  SET @ERRORMESSAGE = 'ERROR ' + CAST(ERROR_NUMBER() AS VARCHAR(10)) + '. LINEA ' + CAST(ERROR_LINE() AS VARCHAR(10)) + '. ' + ERROR_MESSAGE()
  SET @ERRORSEVERITY = ERROR_SEVERITY()  
  SET @ERRORSTATE = ERROR_STATE()  
  GOTO ERROR
END CATCH

SET NOCOUNT OFF

RETURN
ERROR:
  IF XACT_STATE() <> 0 ROLLBACK TRAN
  RAISERROR (@ERRORMESSAGE, @ERRORSEVERITY, @ERRORSTATE)

GO
/****** Object:  StoredProcedure [dbo].[InsertMulta]    Script Date: 14/02/2021 22:17:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[InsertMulta] 
(
 @PATENTE	VARCHAR(50),
 @MOTIVO	VARCHAR(MAX),
 @MONTOMULTA MONEY,
 @INTERES    MONEY

)
AS
/* '===============================================================          
  '   NOMBRE                : [dbo].[InsertMulta]  
  '   FECHA CREACIÓN        : 12-02-2021
  '	  CREADO POR			: YAMELIS CALDERON
  '   CREADO PARA           : PRUEBA TÉCNICA
  '   FUNCIÓN               : INSERTAR MULTAS DEL VEHICULO 
  '   VERSIÓN               : 1.0.0
  '   MODIFICADO EN         : 
  '   MODIFICADO POR        : 
  '   RAZÓN DE MODIFICACIÓN : 
  '===============================================================*/    
SET NOCOUNT ON
DECLARE @ERRORMESSAGE NVARCHAR(4000)  
DECLARE @ERRORSEVERITY INT  
DECLARE @ERRORSTATE INT  

BEGIN TRY
	
	 BEGIN TRAN 
		INSERT INTO MULTA 
		VALUES(@PATENTE ,@MOTIVO , @MONTOMULTA ,@INTERES ,1,CURRENT_TIMESTAMP,'V')
	 COMMIT
	  
END TRY

BEGIN CATCH
    
  SET @ERRORMESSAGE = 'ERROR ' + CAST(ERROR_NUMBER() AS VARCHAR(10)) + '. LINEA ' + CAST(ERROR_LINE() AS VARCHAR(10)) + '. ' + ERROR_MESSAGE()
  SET @ERRORSEVERITY = ERROR_SEVERITY()  
  SET @ERRORSTATE = ERROR_STATE()  
  GOTO ERROR
END CATCH

SET NOCOUNT OFF

RETURN
ERROR:
  IF XACT_STATE() <> 0 ROLLBACK TRAN
  RAISERROR (@ERRORMESSAGE, @ERRORSEVERITY, @ERRORSTATE)

GO
/****** Object:  StoredProcedure [dbo].[InsertPagoMulta]    Script Date: 14/02/2021 22:17:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[InsertPagoMulta] 
(
 @RUTCONDUCTOR	VARCHAR(50), 
 @IDMULTA       INT,
 @MONTOMULTA	MONEY

)
AS
/* '===============================================================          
  '   NOMBRE                : [dbo].[InsertPagoMulta]  'CJCT94','No pago el permiso de circulación',54164,4972
  '   FECHA CREACIÓN        : 12-02-2021
  '	  CREADO POR			: YAMELIS CALDERON
  '   CREADO PARA           : PRUEBA TÉCNICA
  '   FUNCIÓN               : INSERTAR MULTAS DEL VEHICULO 
  '   VERSIÓN               : 1.0.0
  '   MODIFICADO EN         : 
  '   MODIFICADO POR        : 
  '   RAZÓN DE MODIFICACIÓN : 
  '===============================================================*/    
SET NOCOUNT ON
DECLARE @ERRORMESSAGE NVARCHAR(4000)  
DECLARE @ERRORSEVERITY INT  
DECLARE @ERRORSTATE INT  

BEGIN TRY
	BEGIN TRAN	  
			DECLARE @IDPAGO INT
			--INSERTAR DATOS EN EL MAESTRO DE PAGO Y DETALLE SI NO EXISTE UNA MULTA YA ASOCIADA
		IF NOT EXISTS(SELECT * FROM DETALLEPAGO WHERE IdMulta=ISNULL(@IDMULTA,0))
		BEGIN
			INSERT INTO PAGO 
			VALUES (@RUTCONDUCTOR,CURRENT_TIMESTAMP,1,'V')
			SET @IDPAGO = SCOPE_IDENTITY()
			---INSERTAR EL DETALLE DEL PAGO
			INSERT INTO DETALLEPAGO 
			VALUES(@IDPAGO,@IDMULTA,@MONTOMULTA,'V')
	 
			--ACTUALIZAR ESTATUS DE LA MULTA A PAGADA
			UPDATE MULTA 
			SET IDESTATUS = 2 
			WHERE ID = @IDMULTA
		END	
			COMMIT
END TRY

BEGIN CATCH
    
  SET @ERRORMESSAGE = 'ERROR ' + CAST(ERROR_NUMBER() AS VARCHAR(10)) + '. LINEA ' + CAST(ERROR_LINE() AS VARCHAR(10)) + '. ' + ERROR_MESSAGE()
  SET @ERRORSEVERITY = ERROR_SEVERITY()  
  SET @ERRORSTATE = ERROR_STATE()  
  GOTO ERROR
END CATCH

SET NOCOUNT OFF

RETURN
ERROR:
  IF XACT_STATE() <> 0 ROLLBACK TRAN
  RAISERROR (@ERRORMESSAGE, @ERRORSEVERITY, @ERRORSTATE)

GO
USE [master]
GO
ALTER DATABASE [pruebaLegaltec] SET  READ_WRITE 
GO
