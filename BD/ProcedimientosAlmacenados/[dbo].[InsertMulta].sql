USE [pruebaLegaltec]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--si no existe el procedimiento lo crea y si ya existe lo modifica. 
IF  NOT EXISTS (SELECT * FROM SYS.OBJECTS 
                WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[InsertMulta]') 
                AND TYPE IN (N'P', N'PC', N'TF', N'FN'))
                
EXEC('CREATE PROCEDURE [dbo].[InsertMulta] AS BEGIN SET NOCOUNT ON  END')
GO

ALTER  PROCEDURE [dbo].[InsertMulta] 
(
 @PATENTE	VARCHAR(50),
 @MOTIVO	VARCHAR(MAX),
 @MONTOMULTA MONEY,
 @INTERES    MONEY

)
AS
/* '===============================================================          
  '   NOMBRE                : [dbo].[InsertMulta]  'CJCT94','No pago el permiso de circulaci�n',54164,4972
  '   FECHA CREACI�N        : 12-02-2021
  '	  CREADO POR			: YAMELIS CALDERON
  '   CREADO PARA           : PRUEBA T�CNICA
  '   FUNCI�N               : INSERTAR MULTAS DEL VEHICULO 
  '   VERSI�N               : 1.0.0
  '   MODIFICADO EN         : 
  '   MODIFICADO POR        : 
  '   RAZ�N DE MODIFICACI�N : 
  '===============================================================*/    
SET NOCOUNT ON
DECLARE @ERRORMESSAGE NVARCHAR(4000)  
DECLARE @ERRORSEVERITY INT  
DECLARE @ERRORSTATE INT  

BEGIN TRY
	
	 BEGIN TRAN 
		INSERT INTO MULTA 
		VALUES(@PATENTE ,@MOTIVO , @MONTOMULTA ,@INTERES ,1,CURRENT_TIMESTAMP,'V')
	 COMMIT
	  
END TRY

BEGIN CATCH
    
  SET @ERRORMESSAGE = 'ERROR ' + CAST(ERROR_NUMBER() AS VARCHAR(10)) + '. LINEA ' + CAST(ERROR_LINE() AS VARCHAR(10)) + '. ' + ERROR_MESSAGE()
  SET @ERRORSEVERITY = ERROR_SEVERITY()  
  SET @ERRORSTATE = ERROR_STATE()  
  GOTO ERROR
END CATCH

SET NOCOUNT OFF

RETURN
ERROR:
  IF XACT_STATE() <> 0 ROLLBACK TRAN
  RAISERROR (@ERRORMESSAGE, @ERRORSEVERITY, @ERRORSTATE)

