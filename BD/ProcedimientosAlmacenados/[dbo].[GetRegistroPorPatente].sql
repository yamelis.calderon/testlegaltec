USE [pruebaLegaltec]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--si no existe el procedimiento lo crea y si ya existe lo modifica. 
IF  NOT EXISTS (SELECT * FROM SYS.OBJECTS 
                WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[GetRegistroPorPatente]') 
                AND TYPE IN (N'P', N'PC', N'TF', N'FN'))
                
EXEC('CREATE PROCEDURE [dbo].[GetRegistroPorPatente] AS BEGIN SET NOCOUNT ON  END')
GO

ALTER  PROCEDURE dbo.[GetRegistroPorPatente] 
(
 @Patente	varchar(50)
)
AS
/* '===============================================================          
  '   NOMBRE                : [dbo].[GetRegistroPorPatente] 'BZXJ51'  
  '   FECHA CREACI�N        : 12-02-2021
  '	  CREADO POR			: YAMELIS CALDERON
  '   CREADO PARA           : PRUEBA T�CNICA
  '   FUNCI�N               : OBTENER DATOS DEL VEHICULO POR PATENTE
  '   VERSI�N               : 1.0.0
  '   MODIFICADO EN         : 
  '   MODIFICADO POR        : 
  '   RAZ�N DE MODIFICACI�N : 
  '===============================================================*/    
SET NOCOUNT ON
DECLARE @ERRORMESSAGE NVARCHAR(4000)  
DECLARE @ERRORSEVERITY INT  
DECLARE @ERRORSTATE INT  

BEGIN TRY
	
	 SELECT V.Patente ,
		   CONCAT(MR.Descripcion,' ',
		   MD.Descripcion) AS Modelo,
		   V.RutConductor , 
		   CONCAT(C.Nombre, ' ', C.Apellido) AS NombreConductor,
		   P.ValorPermiso, 
		   ISNULL(M.Id,0) AS IdMulta,
		   CASE WHEN  ISNULL(M.Id,0) = 0 THEN '' ELSE CONCAT('Nro. ',V.Patente, ' - ', M.Id) END AS NroMulta,
		   CASE WHEN M.IdEstatus = 2 THEN 0 ELSE ISNULL(M.MontoMulta,0) END AS MontoMulta,
		   CASE WHEN M.IdEstatus  = 2 THEN 0 ElSE ISNULL(M.InteresReajuste,0) END As InteresReajuste,
		   SUM(P.ValorPermiso) + SUM(ISNULL(M.MontoMulta,0)) + SUM(ISNULL(M.InteresReajuste,0)) as Subtotal
	FROM Vehiculo V 
	INNER JOIN MODELO MD ON V.IdModelo   = MD.Id 
	INNER JOIN Marca MR ON MD.IdMarca = MR.Id 
	INNER JOIN Permiso  P ON P.Id = MD.IdPermiso
	INNER JOIN Conductor C ON V.RutConductor = C.RutConductor
	LEFT JOIN Multa M ON M.PatenteVehiculo = V.Patente 
	WHERE V.Patente = @Patente 
	GROUP BY  V.Patente ,MR.Descripcion, MD.Descripcion, V.RutConductor,
	C.Nombre,C.Apellido,P.ValorPermiso,M.MontoMulta,M.InteresReajuste, M.IdEstatus,M.Id
    
END TRY

BEGIN CATCH
    
  SET @ERRORMESSAGE = 'ERROR ' + CAST(ERROR_NUMBER() AS VARCHAR(10)) + '. LINEA ' + CAST(ERROR_LINE() AS VARCHAR(10)) + '. ' + ERROR_MESSAGE()
  SET @ERRORSEVERITY = ERROR_SEVERITY()  
  SET @ERRORSTATE = ERROR_STATE()  
  GOTO ERROR
END CATCH

SET NOCOUNT OFF

RETURN
ERROR:
  IF XACT_STATE() <> 0 ROLLBACK TRAN
  RAISERROR (@ERRORMESSAGE, @ERRORSEVERITY, @ERRORSTATE)

