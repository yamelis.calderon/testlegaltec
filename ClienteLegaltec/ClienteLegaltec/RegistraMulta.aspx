﻿<%@ Page Language="VB" AutoEventWireup="true" CodeFile="RegistraMulta.aspx.vb" Inherits="RegistraMulta" %>
<%@ Register Src="~/Menu.ascx" TagPrefix="uc1" TagName="Menu" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Registrar Multas:: SOAP:: Multas</title>

    
    <script src="../Scripts/jquery-3.3.1.min.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
    

  
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../Content/bootstrap-theme.min.css" rel="stylesheet" />


    <script type="text/javascript">
        function MensajeAlerta( mensaje ) {
           
        alert("¡Datos registrados con éxito!");
    };
</script>
</head>
<body>
    
 <style>
.subtitulo-form {
    color: #1071c3;
}
</style>
  
  <uc1:Menu runat="server" ID="Menu" />
    <form id="form1" runat="server">
       <div class="container theme-showcase" role="main">
           <div class="well" style="margin-top:50px;">
            <div align="center">
                <a  runat="server" href="~/Index.aspx">
                 <img id="Img1" src="~/Imagenes/soap2021.svg" runat="server" />
              </a>
            </div>
            <br />
            <h4 class="subtitulo-form">Registro de Multa</h4>
            <br />
            <div class="row" >
                <div class="col-md-12" style="margin-top:-21px;">
                    <table class="table table-bordered  table-condensed">
                        <tbody>
                            <tr>
                                <th class="titulo-tabla col-md-1" scope="row" >Patente</th>
                                <td class="col-md-4">
                                    <asp:TextBox Id="txtPatente" disabled="true" CssClass="form-control  btn-sm" runat="server" MaxLength="10"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <th class="titulo-tabla col-md-1" scope="row" >Modelo del Vehiculo</th>
                                <td class="col-md-4">
                                    <asp:TextBox Id="txtModelo" disabled="true" CssClass="form-control  btn-sm" runat="server" MaxLength="10"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <th class="titulo-tabla col-md-1" scope="row" >Conductor</th>
                                <td class="col-md-4">
                                    <asp:TextBox Id="txtConductor" disabled="true" CssClass="form-control  btn-sm" runat="server" MaxLength="10"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <th class="titulo-tabla col-md-1" scope="row" > Valor del Permiso</th>
                                <td class="col-md-4">
                                    <asp:TextBox Id="txtValorPermiso" disabled="true" CssClass="form-control  btn-sm" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <th class="titulo-tabla col-md-1" scope="row" >Motivo  de la Multa</th>
                                <td class="col-md-4">
                                    <textarea id="txtMotivo" class="form-control  btn-sm" runat="server" row="2"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <th class="titulo-tabla col-md-1" scope="row" >Monto de la Multa</th>
                                <td class="col-md-4">
                                    <asp:TextBox Id="txtMontoMulta"  CssClass="form-control  btn-sm" runat="server" MaxLength="10"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorMulta" runat="server" ControlToValidate="txtMontoMulta" ErrorMessage="*Ingrese Valores Numericos" ForeColor="Red" ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator>
                                 
                                </td>
                            </tr>
                            <tr>
                                <th class="titulo-tabla col-md-1" scope="row" >Intereses</th>
                                <td class="col-md-4">
                                    <asp:TextBox Id="txtIntereses" CssClass="form-control  btn-sm" runat="server" MaxLength="10"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorIntereses" runat="server" ControlToValidate="txtIntereses" ErrorMessage="*Ingrese Valores Numericos" ForeColor="Red" ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>
                <div class="col-md-12">
                <div class="pull-right">
                  <asp:LinkButton ID="btnRegistroMulta" CssClass="btn btn-info btn-sm pull-right" runat="server" OnClick="btnRegistroMulta_Click"><span class="glyphicon glyphicon-saved"></span> Registrar Multa</asp:LinkButton>   
                </div>
            </div>
        </div>
    </div>
        </div>
    </form>
</body>
</html>
