﻿<%@ Page Language="VB" AutoEventWireup="true" CodeFile="Index.aspx.vb" Inherits="Index" %>
<%@ Register Src="~/Menu.ascx" TagPrefix="uc1" TagName="Menu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Consulta SOAP:: Multas</title>

    <script src="../Scripts/jquery-3.3.1.min.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
    

  
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../Content/bootstrap-theme.min.css" rel="stylesheet" />

<script>
    function MensajeAlerta() {
        alert("Debe Ingresar Datos");
    };
</script>

</head>


<body>
    
<style>
.subtitulo-form {
    color: #1071c3;
}
</style>


<form id="form1" runat="server">
        <uc1:Menu runat="server" ID="Menu" />
   <div class="container theme-showcase" role="main">

        <div class="well" style="margin-top:50px;">
            <div align="center">
                <a  runat="server" href="~/Index.aspx">
                 <img id="Img1" src="~/Imagenes/soap2021.svg" runat="server" />
              </a>
            </div>
            <br />
            <h4 class="subtitulo-form">Consulta de Datos</h4>
            <br />
            <div class="row" >
                <div class="col-md-12" style="margin-top:-21px;">
                    <table class="table table-bordered  table-condensed">
                        <tbody>
                            <tr>
                                <th class="titulo-tabla col-md-1" scope="row" >Patente</th>
                                <td class="col-md-4">
                                    <asp:TextBox Id="txtPatente" CssClass="form-control  btn-sm" runat="server" MaxLength="10"></asp:TextBox> 
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>
                <div class="col-md-12">
                <div class="pull-right">
                    <asp:LinkButton ID="btnConsulta" CssClass="btn btn-primary btn-sm pull-right" runat="server" OnClick="btnConsulta_Click"><span class="glyphicon glyphicon-search"></span> Buscar</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>


        <div class="well" runat="server" id="pnl_consulta" Visible="False">
                    
                    <div class="row">
                                <div class="col-md-12" style="max-height:500px; overflow: auto;">
                                    <div class="table-responsive">
                                        <h4 class="subtitulo-form"> Datos Consultados:</h4>
                                        <hr/>
                                        <asp:GridView ID="gvConsulta" runat="server" CssClass="table table-bordered" CellPadding="3" ForeColor="Black"
                                                      GridLines="Vertical"
                                                      AutoGenerateSelectButton="false"
                                                      AutoGenerateColumns="False" 
                                                      AllowPaging="True" PageSize="10"                                                      
                                                      BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px"
                                                      OnSelectedIndexChanged ="gvConsulta_SelectedIndexChanged"
                                                      ShowHeader="true">
                                            <Columns>
                                                <asp:BoundField DataField="Patente" HeaderText="Patente"/>
                                                <asp:BoundField DataField="Modelo" ItemStyle-CssClass="text-uppercase" HeaderText="Vehiculo"/>
                                                <asp:BoundField DataField="ValorPermiso"  ItemStyle-CssClass="text-uppercase" HeaderText="ValorPermiso"/>
                                                <asp:BoundField DataField="IdMulta" ItemStyle-CssClass="hidden" HeaderText="Nro. de Multa" HeaderStyle-CssClass="hidden"/>
                                                <asp:BoundField DataField="MontoMulta" HeaderText="Registro Multas Impagas"/>
                                                <asp:BoundField DataField="InteresReajuste" ItemStyle-CssClass="text-uppercase" HeaderText="Intereses y Reajuste"/>
                                                <asp:BoundField DataField="SubTotal" ItemStyle-CssClass="text-uppercase" HeaderText="SubTotal"/>
                                                <asp:BoundField DataField="RutConductor" ItemStyle-CssClass="hidden" HeaderText="RutConductor" HeaderStyle-CssClass="hidden" />
                                                <asp:BoundField DataField="NombreConductor" ItemStyle-CssClass="hidden" HeaderText="Conductor"  HeaderStyle-CssClass="hidden" />
                                             </Columns>
                                            <HeaderStyle CssClass="titulo-tabla table-borderless"/>
                                            <PagerSettings Mode="NumericFirstLast"  PageButtonCount="5"></PagerSettings>
                                            <PagerStyle HorizontalAlign="Right" CssClass="GridPager"></PagerStyle>
                                        </asp:GridView>
                                      </div>
                                  </div>
                    </div>
            
        </div>
       <div  id="btn_consulta" class="col-md-12" runat="server"  Visible="false">
                <div class="pull-right">
                  <asp:LinkButton ID="btnRegistroMulta" CssClass="btn btn-info btn-sm pull-right" runat="server" OnClick="btnRegistroMulta_Click"><span class="glyphicon glyphicon-saved"></span> Registrar Multa</asp:LinkButton> 
                  <asp:LinkButton ID="btnPagoMulta" CssClass="btn btn-danger btn-sm pull-right" runat="server" OnClick="btnPagoMulta_Click"><span class="glyphicon glyphicon-credit-card"></span> Pagar Multa</asp:LinkButton>
                 </div>    
      </div>
  </div>
 </form>
</body>
</html>
