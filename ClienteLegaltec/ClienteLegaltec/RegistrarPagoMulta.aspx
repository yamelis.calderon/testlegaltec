﻿<%@ Page Language="VB" AutoEventWireup="true" CodeFile="RegistrarPagoMulta.aspx.vb" Inherits="RegistrarPagoMulta" %>

<%@ Register Src="~/Menu.ascx" TagPrefix="uc1" TagName="Menu" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Registrar Multas:: SOAP:: Multas</title>

    
    <script src="../Scripts/jquery-3.3.1.min.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
    

  
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../Content/bootstrap-theme.min.css" rel="stylesheet" />


 <script type="text/javascript">
     function MensajeAlerta(mensaje) {
         console.log("LLEGOOOO");
        alert(mensaje);
    };
</script>
</head>
<body>
    
 <style>
.subtitulo-form {
    color: #1071c3;
}
</style>
  
  <uc1:Menu runat="server" ID="Menu" />
    <form id="form1" runat="server">
       <div class="container theme-showcase" role="main">
           <div class="well" style="margin-top:50px;">
            <div align="center">
                <a  runat="server" href="~/Index.aspx">
                 <img id="Img1" src="~/Imagenes/soap2021.svg" runat="server" />
              </a>
            </div>
            <br />
            <h4 class="subtitulo-form">Registro Pago de Multa</h4>
            <br />
            <div class="row" >
                <div class="col-md-12" style="margin-top:-21px;">
                    <table class="table table-bordered  table-condensed">
                        <tbody>
                            <tr>
                                <th class="titulo-tabla col-md-1" scope="row" >Patente</th>
                                <td class="col-md-4">
                                    <asp:TextBox Id="txtPatente" disabled="true" CssClass="form-control  btn-sm" runat="server" MaxLength="10"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <th class="titulo-tabla col-md-1" scope="row" >Modelo del Vehiculo</th>
                                <td class="col-md-4">
                                    <asp:TextBox Id="txtModelo" disabled="true" CssClass="form-control  btn-sm" runat="server" MaxLength="10"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <th class="titulo-tabla col-md-1" scope="row" >Conductor</th>
                                <td class="col-md-4">
                                    <asp:TextBox Id="txtConductor" disabled="true" CssClass="form-control  btn-sm" runat="server" MaxLength="10"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <th class="titulo-tabla col-md-1" scope="row" > Valor del Permiso</th>
                                <td class="col-md-4">
                                    <asp:TextBox Id="txtValorPermiso" disabled="true" CssClass="form-control  btn-sm" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <th class="titulo-tabla col-md-1" scope="row" >SubTotal</th>
                                <td class="col-md-4">
                                    <asp:TextBox Id="txtSubTotal" disabled="true"  CssClass="form-control  btn-sm" runat="server" MaxLength="10"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <th class="titulo-tabla col-md-1" scope="row" >Monto de la Multa</th>
                                <td class="col-md-4">
                                    <asp:TextBox Id="txtMontoPago" CausesValidation="true" min="1"  CssClass="form-control  btn-sm" runat="server" MaxLength="10"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorPago" runat="server" ControlToValidate="txtMontoPago" ErrorMessage="*Ingrese Valores Numericos" ForeColor="Red" ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator>                               
                                </td>
                            </tr>
                             <tr id="trIdMulta" runat="server" visible="false">
                                <th class="titulo-tabla col-md-1" scope="row" >Monto de la Multa</th>
                                <td class="col-md-4">
                                    <asp:TextBox Id="txtIdMulta"  ItemStyle-CssClass="hidden" runat="server" MaxLength="10" HeaderStyle-CssClass="hidden"></asp:TextBox>
                                </td>
                            </tr>
                             <tr id="trRut" runat="server" visible="false">
                                <th class="titulo-tabla col-md-1" scope="row" >Monto de la Multa</th>
                                <td class="col-md-4">
                                    <asp:TextBox Id="txtRutConductor"  ItemStyle-CssClass="hidden" runat="server" MaxLength="10" HeaderStyle-CssClass="hidden"></asp:TextBox>
                                </td>
                            </tr>
                            
                        </tbody>
                    </table>

                </div>
                <div class="col-md-12">
                <div class="pull-right">
                  <asp:LinkButton ID="btnPagoMulta" CssClass="btn btn-danger btn-sm pull-right" runat="server" OnClick="btnPagoMulta_Click"><span class="glyphicon glyphicon-credit-card"></span> Pagar Multa</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
        </div>

    </form>
</body>
</html>
