﻿
Imports AppService

Partial Class RegistrarPagoMulta
    Inherits System.Web.UI.Page
    Dim App = New AppServiceSoapClient

    Protected idMulta As String
    Protected patente As String
    Protected modelo As String
    Protected conductor As String
    Protected valorPermiso As Decimal
    Protected rutConductor As String
    Protected conductorCompleto As String
    Protected subTotal As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not (Page.IsPostBack) Then
            SetParametersValue()
            CargarDatos()
        End If


    End Sub

    Public Sub SetParametersValue()
        Try
            idMulta = Request.QueryString("IdMulta")
            patente = Request.QueryString("patente")
            modelo = Request.QueryString("modelo")
            conductor = Request.QueryString("conductor")
            valorPermiso = Integer.Parse(Request.QueryString("valorPermiso"))
            rutConductor = Request.QueryString("rutConductor")
            conductorCompleto = "(" & rutConductor & ") " & conductor
            subTotal = Request.QueryString("subtotal")
        Catch ex As Exception

        End Try

    End Sub
    Public Sub CargarDatos()
        Try
            txtPatente.Text = patente
            txtModelo.Text = modelo
            txtConductor.Text = conductorCompleto
            txtValorPermiso.Text = valorPermiso
            txtSubTotal.Text = subTotal
            txtRutConductor.Text = rutConductor
            txtIdMulta.Text = idMulta
        Catch ex As Exception

        End Try
    End Sub

    Public Sub LimpiarData()
        txtPatente.Text = ""
        txtConductor.Text = ""
        txtModelo.Text = ""
        txtMontoPago.Text = ""
        txtValorPermiso.Text = ""
        txtSubTotal.Text = ""
    End Sub
    Protected Sub btnPagoMulta_Click(sender As Object, e As EventArgs)
        Try
            Dim _mensaje As String = App.InsertPagoMulta(txtRutConductor.Text, Integer.Parse(txtIdMulta.Text), Decimal.Parse(txtMontoPago.Text))
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "script", "MensajeAlerta(" + _mensaje + "')", True)
            LimpiarData()
            Response.Redirect(String.Format("~/Index.aspx"))
        Catch ex As Exception

        End Try
    End Sub
End Class
