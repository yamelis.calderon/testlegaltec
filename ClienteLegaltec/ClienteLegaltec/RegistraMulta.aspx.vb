﻿Imports AppService
Partial Class RegistraMulta
    Inherits System.Web.UI.Page
    Dim App = New AppServiceSoapClient


    Protected idMulta As String
    Protected patente As String
    Protected modelo As String
    Protected conductor As String
    Protected valorPermiso As Decimal
    Protected rutConductor As String
    Protected conductorCompleto As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not (Page.IsPostBack) Then
            SetParametersValue()
            CargarDatos()
        End If

        If (IsNothing(patente)) Then
            Dim _mensaje As String = "Para registrar un pago debe consultar la patente primero"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "script", "MensajeAlerta(" + _mensaje + "')", True)
            LimpiarData()
            Response.Redirect(String.Format("~/Index.aspx"))
        End If


    End Sub

    Public Sub SetParametersValue()
        Try
            idMulta = Request.QueryString("IdMulta")
            patente = Request.QueryString("patente")
            modelo = Request.QueryString("modelo")
            conductor = Request.QueryString("conductor")
            valorPermiso = Decimal.Parse(Request.QueryString("valorPermiso"))
            rutConductor = Request.QueryString("rutConductor")
            conductorCompleto = "(" & rutConductor & ") " & conductor
        Catch ex As Exception

        End Try

    End Sub

    Public Sub CargarDatos()
        Try
            txtPatente.Text = patente
            txtModelo.Text = modelo
            txtConductor.Text = conductorCompleto
            txtValorPermiso.Text = valorPermiso

        Catch ex As Exception

        End Try
    End Sub

    Public Sub LimpiarData()
        txtPatente.Text = ""
        txtConductor.Text = ""
        txtModelo.Text = ""
        txtMontoMulta.Text = ""
        txtMotivo.InnerText = ""
        txtValorPermiso.Text = ""
        txtIntereses.Text = ""
    End Sub
    Protected Sub btnRegistroMulta_Click(sender As Object, e As EventArgs)
        Try
            Dim _mensaje As String = App.InsertMulta(txtPatente.Text, txtMotivo.InnerText, txtMontoMulta.Text, txtIntereses.Text)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "script", "MensajeAlerta()", True)
            LimpiarData()
            Response.Redirect(String.Format("~/Index.aspx"))
        Catch ex As Exception

        End Try
    End Sub


End Class
