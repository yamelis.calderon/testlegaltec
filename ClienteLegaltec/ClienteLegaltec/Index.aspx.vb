﻿Imports System.Data
Imports AppService
Partial Class Index
    Inherits System.Web.UI.Page

    Dim App = New AppServiceSoapClient
    Dim hiddenIdMulta As String
    Dim hiddenPatente As String
    Dim hiddenValorPermiso As String
    Dim hiddenModelo As String
    Dim hiddenMontoMulta As String
    Dim hiddenInteres As String
    Dim hiddenConductor As String
    Dim hiddenSubTotal As String
    Dim hiddenRutConductor As String
    Protected Sub btnConsulta_Click(sender As Object, e As EventArgs)
        If (Trim(txtPatente.Text) <> "") Then
            CargaDatosPatente(txtPatente.Text)
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "script", "MensajeAlerta()", True)
        End If

    End Sub

    Public Sub CargaDatosPatente(ByVal Patente As String)
        Try
            Dim dataConsulta As New DataTable
            dataConsulta = App.GetRegistrosPorPatente(Patente)

            gvConsulta.DataSource = dataConsulta
            gvConsulta.DataBind()

            pnl_consulta.Visible = True
            btn_consulta.Visible = True

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnRegistroMulta_Click(sender As Object, e As EventArgs)
        Try
            GetParametersValue()
            Response.Redirect(String.Format("~/RegistraMulta.aspx?idMulta={0}&patente={1}&modelo={2}&conductor={3}&valorPermiso={4}&rutConductor={5}", hiddenIdMulta, hiddenPatente, hiddenModelo, hiddenConductor, hiddenValorPermiso, hiddenRutConductor))
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnPagoMulta_Click(sender As Object, e As EventArgs)
        Try
            GetParametersValue()
            Response.Redirect(String.Format("~/RegistrarPagoMulta.aspx?idMulta={0}&patente={1}&modelo={2}&conductor={3}&valorPermiso={4}&rutConductor={5}&subtotal={6}", hiddenIdMulta, hiddenPatente, hiddenModelo, hiddenConductor, hiddenValorPermiso, hiddenRutConductor, hiddenSubTotal.Split(",")(0)))
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub gvConsulta_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GetParametersValue()
        Try
            hiddenPatente = gvConsulta.Rows(0).Cells(0).Text
            hiddenModelo = gvConsulta.Rows(0).Cells(1).Text
            hiddenValorPermiso = gvConsulta.Rows(0).Cells(2).Text.Split(",")(0).ToString
            hiddenIdMulta = gvConsulta.Rows(0).Cells(3).Text
            hiddenMontoMulta = gvConsulta.Rows(0).Cells(4).Text
            hiddenInteres = gvConsulta.Rows(0).Cells(5).Text
            hiddenSubTotal = gvConsulta.Rows(0).Cells(6).Text
            hiddenRutConductor = gvConsulta.Rows(0).Cells(7).Text
            hiddenConductor = gvConsulta.Rows(0).Cells(8).Text

        Catch ex As Exception

        End Try
    End Sub


End Class
